resource "aws_instance" "managers" {
  ami           = "ami-005e54dee72cc1d00" # us-west-2
  security_groups = aws_security_group.rules-master.name
  instance_type = var.instance_type
  key_name = "ssh-key"
  count =  var.number_machine
  tags = {
      Name = "managers.[count.index]"
  }
  
}

resource "aws_instance" "workers" {
  ami           = "ami-005e54dee72cc1d00" # us-west-2
  security_groups = aws_security_group.rules-worker.name
  instance_type = "t2.micro"
  key_name = "ssh-key"
  count =  "1"
  tags = {
      Name = "workers.[count.index]"
  }
  
}

resource "aws_key_pair" "deployer" {
  key_name   = "ssh-key"
  public_key = "./ssh.txt"
}

resource "aws_security_group" "rules-master" {
  name        = "rules"
  description = "Allow TLS inbound traffic"
  
  dynamic "ingress" {
    for_each = var.regles-tcp-master
    content {
      from_port = ingress.value
      to_port = ingress.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  resource "aws_security_group" "rules-worker" {
  name        = "rules-worker"
  description = "Allow TLS inbound traffic"
  
  dynamic "ingress" {
    for_each = var.regles-tcp-worker
    content {
      from_port = ingress.value
      to_port = ingress.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  }

  tags = {
    Name = "allow_tls"
  }
}