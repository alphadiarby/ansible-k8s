variable "instance_type" {
    description = "la taille de la machine"
    default = "t2.micro"
  
}

variable "number_machine" {
    type = number
    description = "le nombre de machine désiré"
    default = 2
  
}

variable "regions" {
    description = "la region d'hebergement"
    default = "eu-west-1"
  
}

variable "regles-tcp" {
    type = list(number)
    description = "listes des regles rentrants"
    default = [22, 80, 443, 6443, 2379, 2380, 10250, 10251, 10252]
  
}

variable "regles-tcp-worker" {
    type = list(number)
    description = "listes des regles rentrants"
    default = [22, 80, 443, 10250, 30000-32767]
  
}


